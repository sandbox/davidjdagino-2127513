<?php

/**
 * @file
 * Contains FormAssembly admin settings.
 */

/**
 * Page callback for the FormAssembly settings form.
 */
function formassembly_settings_form() {
  $form = array();

  $key = 'formassembly_oauth_cid';
  $form[$key] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('FormAssembly OAuth Client ID'),
    '#description' => t('The client ID.'),
    '#default_value' => variable_get($key, ''),
  );

  $key = 'formassembly_oauth_secret';
  $form[$key] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('FormAssembly OAuth Client Secret'),
    '#description' => t('The client secret.'),
    '#default_value' => variable_get($key, ''),
  );

  $key = 'formassembly_dev_mode';
  $form[$key] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable development mode'),
    '#description' => t('Use developer.formassembly.com instead of app.formassembly.com for API requests.'),
    '#default_value' => variable_get($key, FALSE),
  );

  $key = 'formassembly_sync_cron';
  $form[$key] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync on cron'),
    '#description' => t('Sync forms when cron is run.'),
    '#default_value' => variable_get($key, FALSE),
  );

  $form['formassembly_sync_now'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync now'),
    '#description' => t('Sync forms after submitting this form.'),
    '#default_value' => FALSE,
  );

  $form['formassembly_reauthorize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reauthorize'),
    '#description' => t('Reauthorize this application and get a new access token.'),
    '#default_value' => FALSE,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validation callback for the FormAssembly settings form.
 */
function formassembly_settings_form_validate($form, &$form_state){
  $values = $form_state['values'];
  $client_id = !empty($values['formassembly_oauth_cid']);
  $client_secret = !empty($values['formassembly_oauth_secret']);
  $sync_now = $values['formassembly_sync_now'];
  $sync_cron = $values['formassembly_sync_cron'];

  // Check that the user has provided oauth credentials.
  if ($sync_now || $sync_cron) {
    if (!$client_id) {
      $error_message = t('Client ID is required to sync.');
      form_set_error('formassembly_oauth_cid', $error_message);
      watchdog('formassembly', $error_message);
    }
    if (!$client_secret) {
      $error_message = t('Client secret is required to sync.');
      form_set_error('formassembly_oauth_secret', $error_message);
      watchdog('formassembly', $error_message);
    }
  }
}

/**
 * Submit handler for the FormAssembly settings form.
 */
function formassembly_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  _formassembly_save_vars($values);

  $client_id = $values['formassembly_oauth_cid'];

  // Deleting this variable is enough to generate a new access_token.
  if ($values['formassembly_reauthorize']) {
    variable_del('formassembly_oauth_access_token');
  }

  $request = new FormAssemblyRequest($client_id);
  $token = $request->authorize();
  $request->setToken($token);

  if ($values['formassembly_sync_now']) {
    $request->syncForms();
  }
}

/**
 * Save values needed later - those that begin with 'formassembly'.
 *
 * @param array $vars
 *   The contents of $form_state['values'].
 */
function _formassembly_save_vars($vars) {
  foreach ($vars as $key => $value) {
    // Only set variables for keys that start with 'formassembly'.
    if (strpos($key, 'formassembly') === 0) {
      variable_set($key, $value);
    }
  }
}
