FormAssembly is a module to synchronize forms from FormAssembly.com to Drupal nodes.

As of the initial commit, the module requires the user to build a content type with
a machine name of 'fa_form' that has the following fields:

- field_fa_form_id (integer, text field, required)
- field_fa_form_markup (long text, text area, required)
- field_fa_form_modified (unix timestamp, text field)

Eventually this content type will be included with the module.

The user will need to configure the module to use the appropriate OAuth credentials
(provided by FormAssembly.com).

Module configuration page: /admin/config/services/formassembly

