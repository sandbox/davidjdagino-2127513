<?php

/**
 * @file FormAssemblyRequest.php
 * Contains class FormAsssemblyRequest.
 * Authorizes the current site and handles API requests to FormAssembly.
 */

include_once 'FormAssemblyForm.php';

class FormAssemblyRequest {

  private $apiHost;
  private $clientId;
  private $clientSecret;
  private $authEndpoint= 'oauth/login';
  private $tokenEndpoint= 'oauth/access_token';
  private $authCallback = 'admin/config/services/formassembly/authorize';
  private $returnUrl;
  private $token;

  public function __construct($client_id, $client_secret = '', $endpoint = '') {
    $api_mode = variable_get('formassembly_dev_mode', FALSE) ? 'developer' : 'app';
    $this->apiHost = 'https://' . $api_mode . '.formassembly.com';
    $this->clientId = $client_id;
    $this->clientSecret = $client_secret;
    $this->authCallback = variable_get('formassembly_auth_callback', FALSE) ?: 'admin/config/services/formassembly/authorize';

    $protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $this->returnUrl = $protocol . $_SERVER['SERVER_NAME'] . '/' . $this->authCallback;
  }

  /**
   * Redirect the user so to authorize the application using OAuth2.0.
   *
   * @return bool|mixed
   *   Returns an access token if the user has already authorized.
   */
  public function authorize() {
    $token = $this->getToken();
    if ($token) {
      return $token;
    }

    $params = array(
      'type' => 'web',
      'client_id' => $this->clientId,
      'redirect_uri' => $this->returnUrl,
      'response_type' => 'code',
    );

    $auth_uri = $this->apiHost . '/' . $this->authEndpoint . '?' . drupal_http_build_query($params);
    drupal_goto($auth_uri);
    return null;
  }

  /**
   * Retrieve an active access_token from the database or request a new one.
   *
   * @param string $code
   *   A hash passed into $_GET['code'] after the user has been redirected back
   *   from the auth portal. Needed for new token requests.
   * @return bool|null
   *   An access token if available or FALSE if something has gone wrong.
   */
  public function getToken($code = '') {
    // Retrieve a previously generated token.
    $token = variable_get('formassembly_oauth_access_token', '');
    if (!empty($token)) {
      return $token;
    }

    // Something went wrong - maybe they ended up at this page by accident?
    if (empty($code)) {
      return FALSE;
    }

    // Prepare to request a new access_token.
    $data = array(
      "grant_type" => "authorization_code",
      "type" => "web_server",
      "client_id" => $this->clientId,
      "client_secret" => $this->clientSecret,
      "redirect_uri" => $this->returnUrl,
      "code" => $code,
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->apiHost . '/' . $this->tokenEndpoint);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    unset($ch);

    $response = json_decode($response);
    if (isset($response->access_token)) {
      $this->setToken($response->access_token);
      variable_set('formassembly_oauth_access_token', $this->token);
      return $this->token;
    }

    watchdog('formassembly', t('Could not retrieve access token.'), array(), WATCHDOG_ERROR);
    return FALSE;
  }

  /**
   * Request all form data from FormAssembly and perform CRUD ops as needed.
   */
  public function syncForms() {
    $forms = array();
    // 1. Get a list of forms
    foreach ($this->getForms() as $form) {
      $forms[$form->Form->id] = $form->Form;
    }
    // Update forms that have changed since the last sync.
    foreach ($forms as $form_data) {
      // Load an existing FA Form entity if one matches $form_data->id or
      // create a new entity otherwise.
      $form = new FormAssemblyForm($form_data->id);

      $entity_last_modified = $form->getModified();
      $data_last_modified = date('U', strtotime($form_data->modified));

      // Update forms that have changed since the last sync.
      if ($entity_last_modified < $data_last_modified) {
        // Get fresh markup and inject in with the other data before update.
        $form_data->markup = $this->getFormMarkup($form_data->id);
        $form->update($form_data);
      }
    }
    watchdog('formassembly', t('Form sync complete.'), array(), WATCHDOG_NOTICE);
  }

  /**
   * Set an access token to use in the current request.
   *
   * @param $token
   *   A valid access_token as returned by FA's API.
   */
  public function setToken($token) {
    $this->token = $token;
  }

  /**
   * Get data about all available forms.
   *
   * @param string $endpoint
   *   The api endpoint to make the request against.
   * @return array
   *   An array of objects representing FormAssembly forms.
   */
  public function getForms($endpoint = 'api_v1/forms/index.json') {
    $request_uri = $this->apiHost . '/' . $endpoint . '?access_token=' . urlencode($this->token);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request_uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    unset($ch);
    $response = json_decode($response);
    return $response->Forms;
  }

  /**
   * Retrieve the HTML for a FormAssembly form.
   *
   * @param $form_id
   *   The id of the form.
   * @return string
   *   HTML representation of the form.
   */
  public function getFormMarkup($form_id) {
    $request_uri = $this->apiHost . '/rest/forms/view/' . $form_id;
    return file_get_contents($request_uri);
  }

}
