<?php

/**
 * @file FormAssemblyForm.php
 * Contains class FormAssemblyForm.
 * Bridges the Drupal Entity API with the FormAssembly API.
 */

class FormAssemblyForm {

  /**
   * @var Unique key assigned by FormAssembly to identify each form.
   */
  private $id;

  /**
   * @var Node id of the current entity.
   */
  private $nid;

  /**
   * @var Node title or "name" as it is keyed by FormAssembly.
   */
  private $title;

  /**
   * @var HTML returned by FormAssembly's REST API, sanitized as necessary.
   */
  private $markup;

  /**
   * @var Unix timestamp since the external data of the entity was last updated.
   */
  private $modified;

  /**
   * @var EntityMetadataWrapper representation of an entity.
   */
  private $entity;

  /**
   * Create a new FaForm object and try to load its corresponding entity from
   * the database or create a new entity if necessary.
   *
   * @param $form_id
   */
  public function __construct($form_id) {
    $this->id = $form_id;
    // Create a new entity if one doesn't already exist.
    if (!$this->load()) {
      $this->create();
    }
  }

  /**
   * Save current values using the EntityMetadataWrapper.
   */
  public function save() {
    // Create a new entity if necessary.
    /* @todo remove? redundant?
    if (empty($this->entity)) {
      $this->create();
    }
    */

    $this->entity->title->set($this->title);
    $this->entity->field_fa_form_id->set($this->id);
    $this->entity->field_fa_form_markup->set(
      array(
        'format' => 'full_html',
        'value' => $this->markup,
      ));
    $this->entity->field_fa_form_modified->set($this->modified);
    $this->entity->save();
    // Keep the node id handy, just in case.
    $this->nid = $this->entity->nid->value();
  }

  /**
   * Get the value on the field_fa_form_modified field.
   *
   * @return string
   *   Unix timestamp.
   */
  public function getModified() {
    return $this->entity->field_fa_form_modified->value();
  }

  /**
   * Set a value on the field_fa_form_modified field.
   *
   * @param string $modified
   *   Unix timestamp.
   */
  public function setModified($modified) {
    $this->entity->field_fa_form_modified->set($modified);
  }

  /**
   * Create a new FA Form entity and an EntityMetadataWrapper object and set to current object.
   */
  private function create() {
    $entity = entity_create('node', array('type' => 'fa_form'));
    $this->entity = entity_metadata_wrapper('node', $entity);
  }

  /**
   * Update member variables and save the new values to the database.
   *
   * @param $data
   *   A slightly modified form of the response from api_v1/forms/index.json.
   */
  public function update($data) {
    $this->markup = $data->markup;
    $this->modified = date('U', strtotime($data->modified));
    $this->title = $data->name;
    $this->save();
  }

  /**
   * Look for an entity that matches the $id of the current object and load its values.
   *
   * @return bool
   *   TRUE if the entity values were loaded successfully, FALSE otherwise.
   */
  public function load() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'fa_form')
      ->fieldCondition('field_fa_form_id', 'value', $this->id)
      // Only 1 node should exist that matches this forms FA id.
      ->range(0,1)
      ->execute();

    if (!empty($result['node'])) {
      $entity = entity_load('node', array_keys($result['node']));
      $this->entity = entity_metadata_wrapper('node', reset($entity));
      $this->nid = $this->entity->nid->value();
      $this->title = $this->entity->title->value();
      $this->markup = $this->entity->field_fa_form_markup->value();
      $this->modified = $this->entity->field_fa_form_modified->value();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Return all FA Form entities that match at least one id in $form_ids.
   *
   * @param $form_ids
   *   An array of FormAssembly Form Ids.
   * @return array
   *   Entities that match at least one id in $form_ids.
   *
   * @todo Determine if this is needed and reduce code duplication with ::load().
   */
  public static function loadAll($form_ids) {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'fa_form')
      ->fieldCondition('field_fa_form_id', 'value', $form_ids, 'IN')
      ->execute();

    $entities = array();
    if (!empty($result['node'])) {
      $entities = entity_load('node', array_keys($result['node']));
    }
    return $entities;
  }

  /**
   * Remove <link> and <meta> tags from the markup.
   *
   * @return string
   *   A sanitized string of HTML markup.
   *
   * @todo Determine if this is needed and implement.
   */
  public function stripLinkTags() {
    $markup = $this->markup;
    $sanitizedMarkup = $markup;
    return $sanitizedMarkup;
  }

}
